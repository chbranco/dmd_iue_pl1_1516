//
//  PresentationViewController.swift
//  SwiftDemo_PL1
//
//  Created by Luis Marcelino on 10/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import UIKit

class PresentationViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var nameField: UITextField!
    
    var myPerson:Person?
//        {
//        didSet {
//            self.nameLabel.text = myPerson?.fullName
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.nameLabel.text = myPerson?.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeNameAction(sender: AnyObject) {
        myPerson?.firstName = nameField.text
        nameLabel.text = myPerson?.fullName
        
        //dismissViewControllerAnimated(true, completion: nil)
        navigationController?.popViewControllerAnimated(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
