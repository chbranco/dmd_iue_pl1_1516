//
//  Person.swift
//  SwiftDemo_PL1
//
//  Created by Catarina Silva on 09/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import Foundation

class Person:NSObject, NSCoding {
    var firstName:String?
    var lastName:String?
    //var nationality = "Portuguese"
    var nationality:String
    
    override init(){
        self.nationality = "Portuguese"
    }
    
    struct Const{
        static let firstName = "firstName"
        static let lastName = "lastName"
        static let nationality = "nationality"
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(firstName, forKey: Const.firstName)
        aCoder.encodeObject(lastName, forKey:Const.lastName)
        aCoder.encodeObject(nationality, forKey: Const.nationality)
    }
    
    required init?(coder aDecoder: NSCoder) {
        firstName = aDecoder.decodeObjectForKey(Const.firstName) as? String ?? ""
        lastName = aDecoder.decodeObjectForKey(Const.lastName) as? String ?? ""
        nationality = aDecoder.decodeObjectForKey(Const.nationality) as? String ?? ""
    }
    
    init(firstName:String, lastName:String, nationality:String){
        self.firstName = firstName
        self.lastName = lastName
        self.nationality = nationality
    }
    
    var fullName: String {
                var auxName:[String] = [ ]
        
                if let firstName = self.firstName {
                    auxName += [firstName]
                }
                if let lastName = self.lastName {
                    auxName += [lastName]
                }
        
                return auxName.joinWithSeparator(" ")
    }
}

