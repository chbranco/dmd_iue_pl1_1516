//
//  MapViewController.swift
//  SwiftDemo_PL1
//
//  Created by Catarina Silva on 19/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {

    
    @IBOutlet weak var map: MKMapView!
    
    let defaults = NSUserDefaults()
    
    var currentLocation = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        map.delegate = self
        
        map.showsUserLocation = true
        
        map.mapType = MKMapType.Satellite
        
        let singleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "didTapMap:")
        singleTap.delegate = self
        
        singleTap.numberOfTapsRequired = 1
        
        map.addGestureRecognizer(singleTap)
    }
    
    func didTapMap (gestureRecognizer: UITapGestureRecognizer) {
        let tapPoint: CGPoint = gestureRecognizer.locationInView(map)
        let touchCoordinate = map.convertPoint(tapPoint, toCoordinateFromView: map)
        
        let latDelta:CLLocationDegrees = 10
        let lonDelta:CLLocationDegrees = 10
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(touchCoordinate, span)
        
        map.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = touchCoordinate
        annotation.title = "Tap location"
        annotation.subtitle = "Where to go next?"
        
        map.addAnnotation(annotation)
        
    }


    
    override func viewDidAppear(animated: Bool) {
        currentLocation.latitude = defaults.doubleForKey("latitude")
        currentLocation.longitude = defaults.doubleForKey("longitude")
        
        let latDelta:CLLocationDegrees = 10
        let lonDelta:CLLocationDegrees = 10
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(currentLocation, span)
        
        map.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        
        
        annotation.coordinate = currentLocation
        annotation.title = "Current location"
        annotation.subtitle = "Where to go next?"
        
        map.addAnnotation(annotation)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
