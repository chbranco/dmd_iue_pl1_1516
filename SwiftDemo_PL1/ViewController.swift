//
//  ViewController.swift
//  SwiftDemo_PL1
//
//  Created by Catarina Silva on 05/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import UIKit
import CoreLocation
import Social

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var nationalityField: UITextField!
    
    @IBOutlet weak var latitudeLabel: UILabel!
    
    @IBOutlet weak var longitudeLabel: UILabel!
    
    let myPerson = Person()
    
    let locationManager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D()
    
    let defaults = NSUserDefaults()
    
    struct imcMeasures {
        var height = 0.0
        var weight = 0.0
    }
    
    var johnDoeMeasures = imcMeasures(height: 1.85, weight: 85)
    
    enum CalculatorOperation {
        case Sum, Subtration
        case Multiplication
        case Division
        
        func description() -> String {
            switch self {
            case .Sum:
                    return "Sum"
            case .Subtration:
                return "Subtration"
            default:
                return "Mutltiplication or Division"
            }
        }
    }
    
    var op1:CalculatorOperation = .Subtration
    var op2 = CalculatorOperation.Sum
    
    //associated values
    enum Result {
        case Sucess (Int, Int)
        case Failure (String)
    }
    var correuBem = Result.Sucess(15, 17)
    var correuMal = Result.Failure("Timeout")
    

    
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var otherLabelText: UILabel!
    
    let nome:String = "Luis";
    var saudacao:String = "Olá";
    
    var s:String?;
    
    let nomes = ["Luis", "Maria", "João"];
    var localidades = ["Leiria", "Lisboa"];
    var morada = [String:String]();
    

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print ("new location")
        let locationObj = locations.last
        let coord = locationObj?.coordinate
        if let c = coord {
            currentLocation = c
            print("lat: \(c.latitude) long:\(c.longitude)")
            defaults.setDouble(currentLocation.latitude, forKey: "latitude")
            defaults.setDouble(currentLocation.longitude, forKey: "longitude")
        }
        
    }
    
    
    @IBAction func savePerson(sender: UIButton) {
        //should protect against empty text fields
        let p = Person(firstName: firstNameField.text!, lastName: lastNameField.text!, nationality: nationalityField.text!)
        
        ArchiveData().savePerson(p)
    }
    
    
    @IBAction func retrievePerson(sender: UIButton) {
        let p = ArchiveData().retrievePerson()
 
        firstNameField.text = p.firstName
        lastNameField.text = p.lastName
        nationalityField.text = p.nationality
        
    }
    
    
    @IBAction func shareAction(sender: UIBarButtonItem) {
        let actionSheet = UIAlertController(title: "", message: "Share your location", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let tweetAction = UIAlertAction(title: "Share on Twitter", style: UIAlertActionStyle.Default){(action) -> Void in
        
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
                //post tweet
                let twitterComposeVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter)

                //define initial tweet, should be checked for size
                twitterComposeVC.setInitialText("My location is \(self.currentLocation.latitude), \(self.currentLocation.longitude)")
                
                self.presentViewController(twitterComposeVC, animated: true, completion: nil)
            } else {
                self.showAlertMessage("You must be logged on Twitter before posting.")
            }
        }
        
        let facebookAction = UIAlertAction(title: "Share on Facebook", style: UIAlertActionStyle.Default){(action) -> Void in
            
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
                let facebookComposeVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                
                facebookComposeVC.setInitialText("My current location: \(self.currentLocation.latitude), \(self.currentLocation.longitude)")
                
                self.presentViewController(facebookComposeVC, animated: true, completion: nil)
            }
            else {
                self.showAlertMessage("You are not connected to your Facebook account.")
            }
        }
        
        let dismissAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel){(action) -> Void in
            
        }
        actionSheet.addAction(tweetAction)
        actionSheet.addAction(facebookAction)
        actionSheet.addAction(dismissAction)
        presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func showAlertMessage (message:String) {
        let alertController = UIAlertController(title: "Error Sharing", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        presentViewController(alertController, animated: true, completion: nil)
        
    }
    @IBAction func getLocation(sender: UIButton) {
        locationManager.startUpdatingLocation()
        latitudeLabel.text = "\(currentLocation.latitude)"
        longitudeLabel.text = "\(currentLocation.longitude)"
        
    }
    
    
    @IBAction func buttonPressed(sender: UIButton) {
        
        myPerson.firstName = firstNameField.text;
        myPerson.lastName = lastNameField.text;
        myPerson.nationality = nationalityField.text ?? "unknow"

        labelText.text = myPerson.fullName
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()

    }
    
    override func viewWillAppear(animated: Bool) {
        firstNameField.text = myPerson.firstName
        lastNameField.text = myPerson.lastName
        
        labelText.text = myPerson.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let controller = segue.destinationViewController as? PresentationViewController {
            controller.myPerson = myPerson
        }
    }

}

