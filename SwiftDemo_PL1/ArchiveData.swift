//
//  ArchiveData.swift
//  SwiftDemo_PL1
//
//  Created by Catarina Silva on 26/11/15.
//  Copyright © 2015 imaginada. All rights reserved.
//

import Foundation

class ArchiveData {
    var documentsPath:NSURL = NSURL()
    var filePath: NSURL = NSURL ()
    
    
    func savePerson(namePerson: Person) {
        documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0])
        print(documentsPath)
        filePath = documentsPath.URLByAppendingPathComponent("Person.data", isDirectory: false)
        print(filePath)
        
        let path = filePath.path!
        
        if NSKeyedArchiver.archiveRootObject(namePerson, toFile: path){
            print("Sucess")
        } else {
            print("Failure")
        }
    }

    func retrievePerson() -> Person {
        var dataToRetrieve = Person()
        documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0])
        filePath = documentsPath.URLByAppendingPathComponent("Person.data", isDirectory: false)
        let path = filePath.path!
        
        if let newData = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as? Person {
            dataToRetrieve = newData
        }
        return dataToRetrieve
    }
}